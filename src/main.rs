use {
    hyper::{
        service::{make_service_fn, service_fn},
        Body, Request, Response, Server, StatusCode,
    },
    std::net::SocketAddr,
    std::path::PathBuf,
};

use std::{
    convert::Infallible,
    sync::{Arc, Mutex, MutexGuard},
};

use hyper::{http, Client, Method};
use hyper_tls::HttpsConnector;
use regex::Regex;

use anyhow::anyhow;
use futures::future::{BoxFuture, FutureExt};
use openpgp::{cert::CertParser, serialize::MarshalInto, Cert};
use openpgp::{parse::Parse, KeyID};
use sequoia_openpgp as openpgp;
use sqlite::Value;

struct Handler {
    connection: Mutex<sqlite::Connection>,
}

fn encoded_part(bytes: &[u8]) -> String {
    use sha1::Sha1;
    let mut m = Sha1::new();
    m.update(bytes);
    let digest = m.digest();
    let bytes = digest.bytes();
    zbase32::encode_full_bytes(&bytes)
}

fn parse_wkd(email: &str) -> (String, String) {
    let parts = email.split('@').collect::<Vec<&str>>();
    let local = parts[0];
    let encoded_local = encoded_part(local.to_lowercase().as_bytes());
    let domain = parts[1];
    (
        format!(
            "https://openpgpkey.{}/.well-known/openpgpkey/{}/hu/{}?l={}",
            domain, domain, encoded_local, local
        ),
        format!(
            "https://{}/.well-known/openpgpkey/hu/{}?l={}",
            domain, encoded_local, local
        ),
    )
}

fn make_req<'a, T>(
    client: &'a hyper::client::Client<T>,
    url: &'a str,
) -> BoxFuture<'a, hyper::Result<Response<Body>>>
where
    T: hyper::client::connect::Connect + Clone + Send + Sync + 'static,
{
    async move {
        let response = client.request(url_to_req(url)).await;

        if let Ok(ref resp) = response {
            if resp.status().as_u16() == 301 {
                let redirect = resp.headers().get("Location").unwrap().to_str().unwrap();
                return make_req(&client, &redirect).await;
            }
        }

        response
    }
    .boxed()
}

fn url_to_req(url: &str) -> http::request::Request<hyper::body::Body> {
    Request::get(url)
        .header("User-Agent", "minics/1 (+https://gitlab.com/wiktor/minics)")
        .body(hyper::body::Body::default())
        .unwrap()
}

async fn wkd_get(email: &str) -> Vec<Cert> {
    let client = Client::builder().build::<_, hyper::Body>(HttpsConnector::new());
    let addresses = parse_wkd(&email);
    let adv_resp = make_req(&client, &addresses.0).await;
    if let Ok(resp) = adv_resp {
        hyper::body::to_bytes(resp.into_body())
            .await
            .map_err(|e| anyhow!(e))
            .and_then(|bytes| CertParser::from_bytes(&bytes).and_then(|parser| parser.collect()))
            .unwrap_or_else(|_| vec![])
    } else {
        let resp = make_req(&client, &addresses.1).await;
        if let Ok(resp) = resp {
            hyper::body::to_bytes(resp.into_body())
                .await
                .map_err(|e| anyhow!(e))
                .and_then(|bytes| {
                    CertParser::from_bytes(&bytes).and_then(|parser| parser.collect())
                })
                .unwrap_or_else(|_| vec![])
        } else {
            vec![]
        }
    }
}

async fn ks_get(kind: &str, id: &str) -> Vec<Cert> {
    let client = Client::builder().build::<_, hyper::Body>(HttpsConnector::new());
    let uri = format!("https://keys.openpgp.org/vks/v1/by-{}/{}", kind, id);
    let resp = make_req(&client, &uri).await;
    if let Ok(resp) = resp {
        hyper::body::to_bytes(resp.into_body())
            .await
            .map_err(|e| anyhow!(e))
            .and_then(|bytes| CertParser::from_bytes(&bytes).and_then(|parser| parser.collect()))
            .unwrap_or_else(|_| vec![])
    } else {
        vec![]
    }
}

async fn pks_get(id: &str) -> Vec<Cert> {
    let client = Client::builder().build::<_, hyper::Body>(HttpsConnector::new());
    let uri = format!(
        "https://keyserver.ubuntu.com/pks/lookup?search=0x{}&op=get",
        id
    );
    let resp = make_req(&client, &uri).await;
    if let Ok(resp) = resp {
        hyper::body::to_bytes(resp.into_body())
            .await
            .map_err(|e| anyhow!(e))
            .and_then(|bytes| CertParser::from_bytes(&bytes).and_then(|parser| parser.collect()))
            .unwrap_or_else(|_| vec![])
    } else {
        vec![]
    }
}

impl Handler {
    pub async fn handle(&self, req: Request<Body>) -> Result<Response<Body>, hyper::Error> {
        let method = req.method().to_string();
        let uri = req.uri().to_string();
        match server_req2(&self.connection, req).await {
            Ok(resp) => {
                println!("{} {} {}", method, uri, resp.status());
                Ok(resp)
            }
            Err(ref error) => {
                println!("{} {} 500: {}", method, uri, error);
                let mut builder = Response::builder();
                let headers = builder.headers_mut().unwrap();
                headers.append(http::header::CONTENT_TYPE, "text/plain".parse().unwrap());
                Ok(builder
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Body::from(error.to_string()))
                    .unwrap())
            }
        }
    }

    pub fn new(connection: sqlite::Connection) -> Self {
        Self {
            connection: Mutex::new(connection),
        }
    }
}

fn put_cert(
    connection: &MutexGuard<sqlite::Connection>,
    cert: &Cert,
) -> Result<String, Box<dyn std::error::Error>> {
    let mut cursor = connection
        .prepare("SELECT cert_id FROM fingerprints WHERE fingerprint = ?")?
        .into_cursor();

    cursor.bind(&[Value::String(cert.fingerprint().to_hex())])?;

    let cert_id = if let Some(row) = cursor.next()? {
        let cert_id = row[0].as_integer().unwrap();

        let mut statement = connection
            .prepare("SELECT raw FROM certs WHERE id = ?")?
            .into_cursor();
        statement.bind(&[Value::Integer(cert_id)])?;
        let row = statement.next()?.unwrap();

        let raw = Cert::from_bytes(row[0].as_binary().unwrap())?;
        let raw = raw.merge_public(cert.clone())?;

        let mut statement = connection.prepare("UPDATE certs SET raw = ? WHERE id = ?")?;
        statement.bind(1, &raw.to_vec()?[..])?;
        statement.bind(2, cert_id)?;
        statement.next()?;

        cert_id
    } else {
        let mut statement = connection.prepare("INSERT INTO certs(raw) VALUES (?)")?;
        statement.bind(1, &cert.to_vec()?[..])?;
        statement.next()?;
        let mut statement = connection.prepare("SELECT last_insert_rowid()")?;
        statement.next()?;
        statement.read::<i64>(0)?
    };

    cert.userids()
        .map(|u| -> Result<(), Box<dyn std::error::Error>> {
            let mut statement = connection.prepare(
                "INSERT OR IGNORE INTO userids(cert_id, user_id, email) VALUES (?, ?, ?)",
            )?;
            statement.bind(1, cert_id)?;
            statement.bind(2, &*String::from_utf8_lossy(u.value()))?;
            if let Ok(Some(email)) = u.email() {
                statement.bind(3, &*email)?;
            } else {
                statement.bind(3, ())?;
            }
            statement.next()?;
            Ok(())
        })
        .collect::<Result<_, _>>()?;

    cert
    .keys()
    .map(|k| k.fingerprint())
    .map(|fingerprint| -> Result<(), Box<dyn std::error::Error>> {
        let mut statement = connection
            .prepare("INSERT OR IGNORE INTO fingerprints(cert_id, fingerprint, longkeyid) VALUES (?, ?, ?)")?;
        statement.bind(1, cert_id)?;
        statement.bind(2, &*fingerprint.to_hex())?;
        statement.bind(3, &*KeyID::from(fingerprint).to_hex())?;
        statement.next()?;
        Ok(())
    })
    .collect::<Result<_, _>>()?;

    Ok(cert.fingerprint().to_hex())
}

async fn server_req2(
    connection: &Mutex<sqlite::Connection>,
    req: Request<Body>,
) -> Result<Response<Body>, Box<dyn std::error::Error>> {
    if req.uri().path() == "/" && req.method() == Method::POST {
        use bytes::Buf;
        let body = hyper::body::aggregate(req.into_body()).await?;
        let connection = connection.lock().unwrap();
        let parser = CertParser::from_reader(body.reader())?;

        let mut infos = vec![];

        for cert in parser {
            if let Ok(cert) = cert {
                infos.push(put_cert(&connection, &cert)?);
            }
        }

        Ok(Response::new(Body::from(infos.join("\n"))))
    } else if req.uri().path() == "/" {
        let connection = connection.lock().unwrap();

        let mut certificates = vec![];

        let mut cursor = connection.prepare("SELECT raw FROM certs")?.into_cursor();
        while let Some(row) = cursor.next()? {
            certificates.extend(row[0].as_binary().unwrap());
        }

        let mut builder = Response::builder();
        let headers = builder.headers_mut().unwrap();
        headers.append(
            http::header::CONTENT_TYPE,
            "application/octet-stream".parse().unwrap(),
        );
        Ok(builder.body(Body::from(certificates))?)
    } else if Regex::new(r"^/[A-Z0-9]{40}$")?.is_match(&req.uri().to_string()) {
        let mut parts = vec![];
        {
            let connection = connection.lock().unwrap();
            let mut cursor = connection
            .prepare("SELECT raw FROM certs JOIN fingerprints ON certs.id = fingerprints.cert_id WHERE fingerprint = ?")?
            .into_cursor();

            cursor.bind(&[Value::String(req.uri().to_string()[1..].to_string())])?;

            while let Some(row) = cursor.next()? {
                parts.extend(row[0].as_binary().unwrap());
            }
        }

        if parts.is_empty() {
            let fpr = &req.uri().to_string()[1..];
            let mut certs = ks_get("fingerprint", fpr).await;
            if certs.is_empty() {
                certs = pks_get(fpr).await;
            }
            let connection = connection.lock().unwrap();
            for cert in certs {
                put_cert(&connection, &cert)?;
                parts.extend(cert.to_vec()?);
            }
        }

        if parts.is_empty() {
            Ok(Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Default::default())?)
        } else {
            Ok(Response::new(Body::from(parts)))
        }
    } else if Regex::new(r"^/[A-Z0-9]{16}$")?.is_match(&req.uri().to_string()) {
        let mut parts = vec![];
        {
            let connection = connection.lock().unwrap();
            let mut cursor = connection
            .prepare("SELECT raw FROM certs JOIN fingerprints ON certs.id = fingerprints.cert_id WHERE longkeyid = ?")?
            .into_cursor();

            cursor.bind(&[Value::String(req.uri().to_string()[1..].to_string())])?;

            while let Some(row) = cursor.next()? {
                parts.extend(row[0].as_binary().unwrap());
            }
        }

        if parts.is_empty() {
            let fpr = &req.uri().to_string()[1..];
            let mut certs = ks_get("keyid", fpr).await;
            if certs.is_empty() {
                certs = pks_get(fpr).await;
            }
            let connection = connection.lock().unwrap();
            for cert in certs {
                put_cert(&connection, &cert)?;
                parts.extend(cert.to_vec()?);
            }
        }

        if parts.is_empty() {
            Ok(Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Default::default())?)
        } else {
            Ok(Response::new(Body::from(parts)))
        }
    } else if Regex::new(r"^/[^@]+@.*[.].*$")?.is_match(&req.uri().to_string()) {
        let mut parts = vec![];
        {
            let connection = connection.lock().unwrap();
            let mut cursor = connection
            .prepare("SELECT raw FROM certs JOIN userids ON certs.id = userids.cert_id WHERE email = ?")?
            .into_cursor();

            cursor.bind(&[Value::String(req.uri().to_string()[1..].to_string())])?;

            while let Some(row) = cursor.next()? {
                parts.extend(row[0].as_binary().unwrap());
            }
        }

        if parts.is_empty() {
            let certs = wkd_get(&req.uri().to_string()[1..]).await;
            let connection = connection.lock().unwrap();
            for cert in certs {
                put_cert(&connection, &cert)?;
                parts.extend(cert.to_vec()?);
            }
        }

        if parts.is_empty() {
            let certs = ks_get("email", &req.uri().to_string()[1..]).await;
            let connection = connection.lock().unwrap();
            for cert in certs {
                put_cert(&connection, &cert)?;
                parts.extend(cert.to_vec()?);
            }
        }

        if parts.is_empty() {
            Ok(Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Default::default())?)
        } else {
            Ok(Response::new(Body::from(parts)))
        }
    } else {
        Ok(Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(Default::default())?)
    }
}

#[tokio::main]
async fn main() {
    //env_logger::init();

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    println!("Listening on: http://{}", addr);

    let dir: PathBuf = std::env::var("XDG_STATE_HOME")
        .unwrap_or_else(|_| std::env::var("HOME").unwrap_or("/root".into()) + "/.local/state")
        .into();
    println!("Config dir: {}", dir.display());

    let connection = sqlite::open(dir.join("minics.db")).unwrap();

    connection.execute(
        "
            CREATE TABLE IF NOT EXISTS certs (id INTEGER PRIMARY KEY, raw BLOB);
            CREATE TABLE IF NOT EXISTS userids(cert_id INTEGER, user_id TEXT, email TEXT, UNIQUE(cert_id, user_id));
            CREATE TABLE IF NOT EXISTS fingerprints(cert_id INTEGER, fingerprint TEXT, longkeyid TEXT, UNIQUE(cert_id, fingerprint));
            ",
    ).unwrap();

    let handler = Arc::new(Handler::new(connection));

    let make_service = make_service_fn(|_| {
        let handler = Arc::clone(&handler);
        async move {
            Ok::<_, Infallible>(service_fn(move |req: Request<Body>| {
                let handler = Arc::clone(&handler);
                async move { handler.handle(req).await }
            }))
        }
    });

    println!("X");
    // Then bind and serve...

    if let Ok(fds) = std::env::var("LISTEN_FDS") {
        use hyperlocal::SocketIncoming;
        use std::os::unix::io::FromRawFd;
        eprintln!("GOT: {}", fds);
        let fds: i32 = fds.parse().unwrap();
        let listener = unsafe { std::os::unix::net::UnixListener::from_raw_fd(fds + 2) };
        listener.set_nonblocking(true).unwrap();
        let listener = tokio::net::UnixListener::from_std(listener).unwrap();

        let builder = Server::builder(SocketIncoming::from_listener(listener));
        let server = builder.serve(make_service);
        if let Err(e) = server.await {
            eprintln!("server error: {}", e);
        }
    }
}
