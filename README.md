# Mini Cert Store

This is a simplified certificate store that allows persisting certificates ("public keys") and then querying and fetching them.

Minics is used as a certificate backend for [git GnuPG shim](https://gitlab.com/wiktor-k/git-gpg-shim) when verifying commit signatures.

This is *not yet ready for production*.

## Design

The minics implements just basic certificate handling, merging and fetching by various identifiers.

The store automatically fetches certificates from Web Key Directory and keys.openpgp.org during certificate lookup.

Out of scope is application of policies or displaying or storing data that relies on policies (e.g. subkey flags).
These out of scope items are supposed to be implemented as separate services that may use this one as a backend.

## Run

    cargo run

This will create `minics.db` file in `$XDG_STATE_HOME` directory and start an HTTP server on port 3000.
Note that there are no database migrations supported so during project code update you may need to remove the database file.

## Populate with keys

Send your certificates to the root path.

For example to send your GnuPG keyring use:

    gpg --export | curl --data-binary @- localhost:3000

You will get a list of imported certificate fingerprints as a result.

This command automatically merges in new data from certificates and updates internal tables.
Thus there is only one command to update and refresh the data.

Note that uploading keys is not necessary for normal use if keys are reachable via Web Key Directory or keys.openpgp.org.

## Querying certificates

All of the query commands can return multiple certificates if there are multiple matches.
Returned data is always binary OpenPGP byte stream.

### Get certificates by fingerprint

Pass the fingerprint as the sole path parameter:

    curl -s localhost:3000/653909A2F0E37C106F5FAF546C8857E0D8E8F074 | gpg --import

Note that this may return multiple certificates in some cases (e.g. the same signing subkey is used in two certificates).

When no matches are found in the local database keys.openpgp.org is queried and the resulting keys are added to the local database and returned to the caller.

### Get certificates by long key ID

This is the same as with fingerprint and can also potentially return multiple certificates:

    curl -s localhost:3000/6C8857E0D8E8F074 | gpg --import

This also queries keys.openpgp.org when needed.

### Get certificates by e-mail

Additionally to lookups by fingerprints and key IDs a simple e-mail lookup is also supported:

```
$ curl -s localhost:3000/wiktor@... | gpg --import
gpg: key 6C8857E0D8E8F074: 239 signatures not checked due to missing keys
gpg: key 6C8857E0D8E8F074: "Wiktor Kwapisiewicz <wiktor@...>" not changed
gpg: Total number processed: 1
gpg:              unchanged: 1
```

When no e-mail hits are found locally first Web Key Directory is checked (advanced, then direct) and if no matches are found in the WKD then keys.openpgp.org e-mail lookup is performed.

### Browse the list of certificates

Fetching the root path returns a JSON formatted list of all certificates along with minimal info about them:

```json
$ curl -s localhost:3000 | head -n 30
{
  "certificates": [
    {
      "url": "http://localhost:3000/653909A2F0E37C106F5FAF546C8857E0D8E8F074",
      "fingerprints": [
        "653909A2F0E37C106F5FAF546C8857E0D8E8F074",
        "EF1EE0FA9420F804FDEFC02697FDEF34DAB8F82B",
        "F9FE4648F5F4A9CE23BA298860D2F50529E2DE4F",
        "DD977564E03AC260B414A1A53B6DFCC964CFEBC4",
        "9C05936329761301B055BBBBB9BEAFCADA89714C",
        "4176054F590205856C36490AE88D0649795CAC0B",
        "C200C506497B07C07A41AC31D27076C34BC6B8C6",
        "DB7D5F272EAD8CE85490BAE2F5DAB3B2F1143313",
        "9C4C6BB520FC2CE3AD8AA552AB77771ACFC6E4E6",
        "9FB340AED954473E51683D156A304C622D907259",
        "42CCBA7DCD0E3A818425CCAB7802C0AFF93B9048",
        "D92D260EDA105F50057FA137413900012F8DC928",
        "59A29DEA8D37388C656863DFB97A1EE09DB417EC",
        "3BA4FE02BF714A7789CB2E0051F23D6C0529CE0A",
        "E7E2B84A36457BEA3F43692DE68BE3B312FA33FC",
        "F99A81E09CD8814B571DBF4AEB0BE68CD9CF08F1"
      ],
      "userIds": [
        "Wiktor Kwapisiewicz <wiktor@metacode.biz>"
      ]
    },
    {
      "url": "http://localhost:3000/C100346676634E80C940FB9E9C02FF419FECBE16",
      "fingerprints": [
        "C100346676634E80C940FB9E9C02FF419FECBE16",
```

Note that this output format is highly experimental.

## Further work

1. There is no way to delete data (e.g. remove a certificate).
2. List of certificates (`/`) should use paging.
3. Limited support for HKP protocol.
4. Support for more complex queries: e.g. listing all key IDs in one query (for performance reasons).
